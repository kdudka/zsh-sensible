#
# Type `zrestart` to safely apply changes after editing this file.
#
zrestart() {
  local zshrc=${ZDOTDIR:-$HOME}/.zshrc
  print -P "Validating %U$zshrc%u..."
  zsh -nf $zshrc ||
    return

  print -P 'Restarting Zsh...'
  zsh -l &&
    exit
}


##
# History settings
# See http://zsh.sourceforge.net/Doc/Release/Parameters.html#Parameters-Used-By-The-Shell
# and http://zsh.sourceforge.net/Doc/Release/Options.html#History
#

# File in which to save command line history
HISTFILE=${ZDOTDIR:-$HOME}/.zhistory

SAVEHIST=10000                  # Max number of history lines to save to file
HISTSIZE=$(( 1.2 * SAVEHIST ))  # Max number of history lines to keep in memory
setopt EXTENDED_HISTORY         # Save each command's time & duration to history.
setopt INC_APPEND_HISTORY_TIME  # Save history whenever a command finishes, not just on shell exit.
setopt HIST_EXPIRE_DUPS_FIRST   # Delete duplicate history lines first, when running out of space.
setopt HIST_IGNORE_DUPS         # Don't store last command line if identical to previous one.


##
# Prompt settings
# See http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html#Expansion-of-Prompt-Sequences
# and http://zsh.sourceforge.net/Doc/Release/Parameters.html#Parameters-Used-By-The-Shell
#

setopt TRANSIENT_RPROMPT  # Remove right side of each prompt after accepting command line.

# Shown at the end of input that doesn't end in a newline character
PROMPT_EOL_MARK='%S%F{cyan}%#%s%f'

# Run this whenever we change dirs, before the prompt.
.zshrc.pwd() {
  # `print -P` lets you use prompt escape codes.
  # %B: Bold/Bright on;  %b: Bold/Bright on  (depending on your terminal)
  # %F{x}: set Foreground color to x;  %f: set Foreground color to default
  # %~: present working dir with ~prefix replacement
  print -P - '%B%F{blue}%~%b%f'
}
add-zsh-hook chpwd .zshrc.pwd
setopt CD_SILENT  # Suppress built-in auto-printing of dir names.

# Primary prompt, left side
# %(?,x,y): if exit status of last command is zero (success), then x, else y
# %S: Standout mode on;  %s: standout mode off
# %#: if user is privileged, then `#`, else `%`;  %n: user Name;  %m: Machine name
PS1='%(?,%F{green},%F{red}%S)%#%f%s '

# Primary prompt, right side
autoload -Uz add-zsh-hook vcs_info
.zshrc.vcs_info() {
  vcs_info  # Generate Git info.
  RPS1="$vcs_info_msg_0_ %F{cyan}%n%f@%F{magenta}%m%f"  # Add it to prompt.
}
add-zsh-hook precmd .zshrc.vcs_info # Run this before each command line.

# Enable %c and %u. (See below.)
zstyle ':vcs_info:*' check-for-changes yes
zstyle ':vcs_info:*' stagedstr '+'
zstyle ':vcs_info:*' unstagedstr '*'

# Shown when in a repo.  %r: Repo Root dir;  %c: Committed changes;  %u: Uncomitted changes
# %b: Branch;  %B: Bold/Bright on;  %%b: Bold/Bright off  (depending on your terminal)
zstyle ':vcs_info:*' formats '%B%F{blue}%r%%b%f#%B%F{cyan}%b%%b%F{green}%c%F{red}%u%f'

# Shown during rebase, merge, etc.  %a: current Action
zstyle ':vcs_info:*' actionformats '%B%F{blue}%r%%b%f#%B%F{red}%a%%b%F{green}%c%F{red}%u%f'

zstyle ':vcs_info:*' enable git # Disable all other VCS backends, to improve performance.

# Continuation prompt. Shown when an incomplete command has been entered.
# %(x_,y,z): if at least x shell constructs are open, then x, else y
# $'' strings let you use `print` escape codes, such as \t for tab.
# %^: names of open shell constructs, from innermost to outermost
PS2=${(j::):-%({1..9}_,$'\t',)} # Indent left side by one tab for each open shell construct.
RPS2='%F{red}%^%f (Press %SCtrl-Q%s to return to PS1)'  # right side
zle -A push-line{-or-edit,}  # Control-Q
unsetopt FLOW_CONTROL         # Allow ^Q and ^S to be used for key bindings.

# Spelling correction prompt.  %R: word to correct;  %r: suggested correction
SPROMPT='Correct %B%F{red}%U%R%b%f%u to %F{green}%r%f? [%Sy%ses|%Sn%so|%Se%sdit|%Sa%sbort] '
setopt CORRECT  # Enable spelling correction of commands.

# Debugging prompt. Enabled with `setopt verbose xtrace`.
# %e: Evaluation depth;  %N: script/function Name;  %I: line number in source file
# %_: names of open shell constructs, from outermost to innermost
PS4=$'#->%(?,%F{green},%F{red}%S)%?%f%s; %e:%F{green}%1N%f:%I %(1_,%F{yellow}%K{black}%_%f%k ,)'


##
# Key bindings
# See http://zsh.sourceforge.net/Doc/Release/Zsh-Line-Editor.html#Zle-Builtins
#

bindkey -e      # Use `emacs` as the main keymap.
typeset -A key  # Declare `$key` table, without overriding any existing values.

# Check if the terminal supports setting application mode.
zmodload zsh/terminfo
if [[ -n "$terminfo[smkx]" ]]; then
  # Enable application mode, so we can use terminfo codes.
  zle -N zle-line-init .zshrc.app-mode; .zshrc.app-mode() { echoti smkx }
  if [[ -n "$terminfo[rmkx]" ]]; then
    zle -N zle-line-finish .zshrc.raw-mode; .zshrc.raw-mode() { echoti rmkx }
  fi

  # Assign each value only if it has not been defined earlier.
  : ${key[Up]:=$terminfo[kcuu1]}    ${key[Down]:=$terminfo[kcud1]}
  : ${key[Right]:=$terminfo[kcuf1]} ${key[Left]:=$terminfo[kcub1]}
  : ${key[End]:=$terminfo[kend]}    ${key[Home]:=$terminfo[khome]}
  : ${key[PageUp]:=$terminfo[kpp]}  ${key[PageDown]:=$terminfo[knp]}
  : ${key[Return]:=$terminfo[cr]}   ${key[Delete]:=$terminfo[kdch1]}
  : ${key[Tab]:=$terminfo[ht]}      ${key[Backtab]:=${terminfo[kcbt]:-$terminfo[cbt]}}
fi

# Fill in any remaining blank values with common defaults.
: ${key[Up]:='^[[A'}   ${key[Down]:='^[[B'}    ${key[Right]:='^[[C'}   ${key[Left]:='^[[D'}
: ${key[End]:='^[[F'}  ${key[Home]:='^[[H'}    ${key[PageUp]:='^[[5~'} ${key[PageDown]:='^[[6~'}
: ${key[Return]:='\r'} ${key[Delete]:='^[[3~'} ${key[Tab]:='^I'}       ${key[Backtab]:='^[[Z'}

# Alt-H: Open `man` page of current command.
unalias run-help 2>/dev/null; autoload -Uz run-help{,-git,-ip,-openssl,-p4,-sudo,-svk,-svn}

# Alt-Shift-/: Show definition of current command.
unalias which-command 2>/dev/null; autoload -Uz which-command; zle -N which-command

bindkey '^Z'    undo  # Control-Z
bindkey '^[^Z'  redo  # Alt-Control-Z

# Up/Down: Cycle through history lines that start with same word as command line.
zle -A up-line-or-{search,history}
zle -A down-line-or-{search,history}

# Alt-Right/Left: Move cursor right/left one word.
bindkey "^[$key[Right]" forward-word
zle -A {emacs-,}forward-word
bindkey "^[$key[Left]" backward-word
zle -A {emacs-,}backward-word
WORDCHARS=''  # Characters treated as part of words (in addition to letters & numbers)

bindkey "$key[Home]" beginning-of-line
bindkey "$key[End]" end-of-line

# Delete: Delete one character to the right or, if that's not possible, list completions.
# If the list is too long to fit on screen, then start type-ahead search.
bindkey "$key[Delete]" delete-char-or-list
zle -C {,.}delete-char-or-list _generic
zstyle ':completion:delete-char-or-list:*:default' menu yes select=long-list interactive

# Alt-Delete: Cut the word to the right of the cursor. (opposite of Alt-Backspace)
bindkey "^[$key[Delete]" kill-word

bindkey '^U' backward-kill-line  # Control-U: Cut text to beginning of line.


##
# Completion settings
# See http://zsh.sourceforge.net/Doc/Release/Completion-System.html#Standard-Styles
#

ZLE_REMOVE_SUFFIX_CHARS=$' \t\n;' # Characters that remove completion suffixes.
ZLE_SPACE_SUFFIX_CHARS=$'&|'      # Characters that instead replace them with a space.
LISTMAX=0                         # Immediately list completions whenever they fit on screen.
unsetopt LIST_AMBIGUOUS           # Always list completions when there is more than one match.
unsetopt LIST_BEEP                # Don't beep when listing completions.
setopt LIST_PACKED                # Lay out completions as compactly as possible.

# Initialize the completion system.
# See http://zsh.sourceforge.net/Doc/Release/Completion-System.html#Use-of-compinit
# and http://zsh.sourceforge.net/Doc/Release/Zsh-Modules.html#The-zsh_002fcomplist-Module
zmodload zsh/complist
autoload -Uz compinit
compinit -d ${ZDOTDIR:-$HOME}/.zcompdump

# Enable completion caching & store cache files in a sensible place.
zstyle ':completion:*' cache-path ${ZDOTDIR:-$HOME}/.zcompcache
zstyle ':completion:*' use-cache yes

# Tab/Shift-Tab: Start type-ahead completion.
#   * Tab:        Insert substring shown by `interactive: <prefix>[]<suffix>`.
#   * Shift-Tab:  Accept selection & start next completion.
#   * Arrow keys: Change selection.
#   * Enter:      Accept selection & stop completing.
zstyle ':completion:(|reverse-)menu-complete:*:default' menu yes select interactive
bindkey "$key[Tab]" menu-complete
zle -C {,.}menu-complete _generic
bindkey "$key[Backtab]" reverse-menu-complete
zle -C {,.}reverse-menu-complete _generic
bindkey -M menuselect -s "$key[Backtab]" "$key[Return]$key[Tab]"
bindkey -M menuselect "^[$key[Up]"     vi-backward-blank-word # Alt-Up: Previous completion group
bindkey -M menuselect "^[$key[Down]"   vi-forward-blank-word  # Alt-Down: Next completion group
bindkey -M menuselect "$key[PageUp]"   backward-word          # Page Up
bindkey -M menuselect "$key[PageDown]" forward-word           # Page Down

# Try these completers in order until one of them succeeds:
# _oldlist: try to reuse previous completions
# _expand: try to substitute expansions (See http://zsh.sourceforge.net/Doc/Release/Expansion.html)
# _complete: try regular completions
# _correct: try _complete with spelling correction
# _complete:-fuzzy: try _complete with a fuzzy matcher
# _history: try history words
# _ignored: show completions that have been hidden so far
zstyle ':completion:*' completer \
  _oldlist _expand _complete _correct _complete:-fuzzy _history _ignored

# (Shift-)Tab uses old list, if visible.
zstyle ':completion:(|reverse-)menu-complete:*' old-list shown
zstyle ':completion:*' old-list never # Other widgets always generate new completions.

# Even when _expand succeeds, if there was only one choice, continue to the next completer.
zstyle ':completion:*:expand:*' accept-exact continue

zstyle ':completion:*' remove-all-dups yes  # Hide duplicate history words.

# Each 'string in quotes' is tried in order until one them succeeds.
# r:|[.]=** does .bar -> foo.bar
# r:?|[-_]=** does f-b -> foo-bar and F_B -> FOO_BAR
# l:?|=[-_] does foobar -> foo-bar and FOOBAR -> FOO_BAR
# m:{[:lower:]-}={[:upper:]_} does foo-bar -> FOO_BAR (but not vice versa)
# r:|?=** does fbr -> foo-bar and bar -> foo-bar
zstyle ':completion:*:complete:*' matcher-list \
  'r:|[.]=** r:?|[-_]=** l:?|=[-_] m:{[:lower:]-}={[:upper:]_}'
zstyle ':completion:*:complete-fuzzy:*' matcher-list \
  'r:|?=** m:{[:lower:]}={[:upper:]}'
zstyle ':completion:*:options' matcher 'b:-=+'  # For options, do -x -> +x

# Hide certain completions, unless we get no other completions.
zstyle ':completion:*' prefix-needed yes            # Params & functions starting with `.` or `_`
zstyle ':completion:*:users' ignored-patterns '_*'  # Users starting with `_`

zstyle ':completion:*' group-name ''                # Organize completions in groups.
zstyle ':completion:*:descriptions' format '%B%d%b' # Show group titles in bold.
zstyle ':completion:*:expansions' format "%B%d%b for '%o'"
zstyle ':completion:*:corrections' format '%B%d%b for %F{red}%o%f (%e typo(s))'
zstyle ':completion:*' list-dirs-first yes          # Group dirs separately from other files.
zstyle ':completion:*' separate-sections yes        # Show `man` and `dict` sections.
zstyle ':completion:*' insert-sections yes          # Insert `man` sections.
zstyle ':completion:*:default' list-prompt ''       # Enable completion list scrolling.

# Add color to messages and warnings.
zstyle ':completion:*:messages' format "%F{r}%d%f"
zstyle ':completion:*:warnings' format '%F{r}No %d found.%f'

# Initialize `$color` table.
autoload -Uz colors
colors
unfunction colors

# Highlight next differentiating character.
zstyle ':completion:*:default' show-ambiguity "$color[black];$color[bg-yellow]"
zstyle ':completion:*' list-separator ''  # Eliminate some false positives.

# Reuse $LS_COLORS for completion highlighting and add additional colors.
# See http://zsh.sourceforge.net/Doc/Release/Zsh-Modules.html#Colored-completion-listings
# zstyle -e: Evaluate the given code each time this style gets checked.
# See end of section http://zsh.sourceforge.net/Doc/Release/Completion-System.html#Overview-1
zstyle -e ':completion:*:default' list-colors .zshrc.list-colors
.zshrc.list-colors() {
  reply=(
    ${(s.:.)LS_COLORS}
    "ma=$color[bold];$color[white];$color[bg-blue]" # Menu selection
    "sa=$color[green];$color[underline]"            # Suffix aliases
    "(global-aliases|parameters)=*=$color[cyan]"
    "(aliases|executables|functions|commands|builtins|jobs)=*=$color[green]"
    "(reserved-words)=*=$color[yellow];$color[bg-black]"
    "(glob(flags|quals)|modifiers)=*=$color[blue]"
  )
}


##
# Directory settings
# See http://zsh.sourceforge.net/Doc/Release/Options.html#Changing-Directories
#

setopt AUTO_CD            # Change dirs simply by typing a dir name. No `cd` required.
setopt AUTO_PUSHD         # Auto-store each dir we visit in the dir stack.
setopt PUSHD_SILENT       # Don't print the dir stack when using `pushd`.
setopt PUSHD_IGNORE_DUPS  # Discard duplicate entries.

bindkey -s '^[-' "^Qpushd -1$key[Return]"   # Alt-Minus:  Back to previous dir stack entry.
bindkey -s '^[=' "^Qpushd +0$key[Return]"   # Alt-Equals: Forward to next dir stack entry.
setopt PUSHD_MINUS                          # `~-` + Tab: Select a dir stack entry.
bindkey -s "^[$key[Up]" "^Q..$key[Return]"  # Alt-Up:     Up to parent dir.
bindkey "^[$key[Down]" menu-select          # Alt-Down:   Select a local dir or file.
zle -C {,}menu-select _generic
zstyle ':completion:menu-select:*:default'  menu yes select interactive
zstyle ':completion:menu-select:*:-command-:*' tag-order \
  'globbed-files executables (|local-)directories suffix-aliases' -
zstyle ':completion:menu-select:*' tag-order '(|*-)directories (|*-)files' -

# Auto-save last 20 dirs to file whenever we change dirs.
# See http://zsh.sourceforge.net/Doc/Release/User-Contributions.html#Recent-Directories
autoload -Uz add-zsh-hook chpwd_recent_dirs
add-zsh-hook chpwd chpwd_recent_dirs
zstyle ':chpwd:*' recent-dirs-file ${ZDOTDIR:-$HOME}/.chpwd-recent-dirs

# On startup, initialize dir stack from file.
autoload -Uz chpwd_recent_filehandler
chpwd_recent_filehandler
cd $reply[1]
dirs $reply[@] >/dev/null


##
# Miscellaneous shell options
# See http://zsh.sourceforge.net/Doc/Release/Options.html
#

setopt GLOB_STAR_SHORT    # Use `**` for recursive globbing; `***` to also traverse symlinks.
setopt NUMERIC_GLOB_SORT  # Sort numeric matches numerically, not lexicographically.
setopt EXTENDED_GLOB      # Enable additional pattern matching operators.
# See http://zsh.sourceforge.net/Doc/Release/Expansion.html#Filename-Generation

unsetopt CLOBBER            # Don't let `>` overwrite files. Use `>|` instead.
setopt INTERACTIVE_COMMENTS # Allow comments on the command line.


##
# Other settings
#

# Make each entry in these Unique (that is, remove duplicates).
# See http://zsh.sourceforge.net/Doc/Release/Shell-Builtin-Commands.html#index-typeset
typeset -gU PATH path FPATH fpath CDPATH cdpath MANPATH manpath

# Add some basic command line highlighting.
# See http://zsh.sourceforge.net/Doc/Release/Zsh-Line-Editor.html#Character-Highlighting
zle_highlight=(
  isearch:fg=black,bg=yellow  # Matched search text
  paste:none                  # Yanked/pasted text
  region:bg=blue              # Selected text
  special:fg=cyan,bold        # Unprintable characters
  suffix:bg=blue              # Auto-removable suffix inserted by completion
)

# Use suffix aliases to associate file extensions with commands.
# This way, you can open a file simply by typing its name.
# See http://zsh.sourceforge.net/Doc/Release/Shell-Builtin-Commands.html#index-alias
READNULLCMD='less'      # Makes `< foo` do `less < foo`.
alias -s txt='<' md='<' # .txt and .md files
alias -s log='tail -f'  # .log files
